﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string markup = File.ReadAllText("MarkupTest.txt", Encoding.UTF8).Trim();
            File.WriteAllText("MarkupTest.html", ParseToHtml(markup));
            File.WriteAllText("MarkupTest.tex", ParseToLatex(markup));

            Benchmark(markup, 2000);

            string markupLong = string.Empty;
            while (markupLong.Length < 350000)
                markupLong = markupLong + markup;

            Benchmark(markupLong, 25);

            Console.ReadKey();
        }

        private static string ParseToHtml(string markup)
        {
            Lexer lexer = new Lexer(markup);

            Parser parser = new Parser(lexer);
            Document document = parser.Parse();

            HtmlVisitor visitor = new HtmlVisitor();
            visitor.Visit(document);

            return visitor.Html;
        }

        private static string ParseToLatex(string markup)
        {
            Lexer lexer = new Lexer(markup);

            Parser parser = new Parser(lexer);
            Document document = parser.Parse();

            LatexVisitor visitor = new LatexVisitor();
            visitor.Visit(document);

            return visitor.Latex;
        }

        private static void Benchmark(string markup, int iterations)
        {
            Console.WriteLine(string.Format("=== Benchmarking with markup of length {0} ", markup.Length).PadRight(60, '='));
            Console.WriteLine();

            Lexer lexer = null;
            List<IToken> tokens = null;
            Parser parser = null;
            Document document = null;
            HtmlVisitor htmlVisitor = null;

            DateTime timestamp1 = DateTime.Now;

            for (int i = 0; i < iterations; i++)
            {
                lexer = new Lexer(markup);
                tokens = lexer.Tokens.ToList();
            }

            DateTime timestamp2 = DateTime.Now;

            for (int i = 0; i < iterations; i++)
            {
                parser = new Parser(tokens);
                document = parser.Parse();
            }

            DateTime timestamp3 = DateTime.Now;

            for (int i = 0; i < iterations; i++)
            {
                htmlVisitor = new HtmlVisitor();
                htmlVisitor.Visit(document);
            }

            DateTime timestamp4 = DateTime.Now;

            TimeSpan total = timestamp4 - timestamp1;
            TimeSpan lexing = timestamp2 - timestamp1;
            TimeSpan parsing = timestamp3 - timestamp2;
            TimeSpan visiting = timestamp4 - timestamp3;

            Console.WriteLine("    Total:      {1,8:0.00}ms   {0,8:0.00}%", 100.0, total.TotalMilliseconds / (double)iterations);
            Console.WriteLine("    " + string.Empty.PadLeft(34, '-'));
            Console.WriteLine("    Lexing:     {1,8:0.00}ms   {0,8:0.00}%", lexing.TotalSeconds / total.TotalSeconds * 100.0, lexing.TotalMilliseconds / iterations);
            Console.WriteLine("    Parsing:    {1,8:0.00}ms   {0,8:0.00}%", parsing.TotalSeconds / total.TotalSeconds * 100.0, parsing.TotalMilliseconds / iterations);
            Console.WriteLine("    Visiting:   {1,8:0.00}ms   {0,8:0.00}%", visiting.TotalSeconds / total.TotalSeconds * 100.0, visiting.TotalMilliseconds / iterations);

            Console.WriteLine();
        }
    }
}