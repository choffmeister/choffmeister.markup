﻿using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace Choffmeister.Markup.Test2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            tbInput.Text = File.ReadAllText("MarkupTest.txt");
        }

        private void tbInput_TextChanged(object sender, TextChangedEventArgs e)
        {
            Lexer lexer = new Lexer(tbInput.Text);
            Parser parser = new Parser(lexer);
            HtmlVisitor visitor = new HtmlVisitor(new TestLinkResolver());
            visitor.Visit(parser.Parse());

            string html = visitor.Html;
            tbOutput.Text = html;
            wbOutput.DocumentText = html;
        }
    }

    public class TestLinkResolver : IUrlResolver
    {
        public string Resolve(string url)
        {
            return "http://" + url;
        }
    }
}