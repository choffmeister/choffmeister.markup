﻿namespace Choffmeister.Markup
{
    public interface IUrlResolver
    {
        string Resolve(string url);
    }
}