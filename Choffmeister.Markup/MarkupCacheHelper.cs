﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Choffmeister.Markup
{
    public static class MarkupCacheHelper
    {
        public static string GetHash(string content)
        {
            return BitConverter.ToString(SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(content ?? string.Empty))).Replace("-", "").ToLower();
        }

        public static string GetPath(string group, string hash, string extension)
        {
            string seperator = Path.DirectorySeparatorChar.ToString();
            string baseDir = Path.GetTempPath();

            string dir1 = Path.Combine(baseDir, group, hash.Substring(0, 2));
            string dir2 = Path.Combine(baseDir, group, hash.Substring(0, 2), hash.Substring(2, 2));
            string dir3 = Path.Combine(baseDir, group, hash.Substring(0, 2), hash.Substring(2, 2), hash.Substring(4, 2));

            if (!Directory.Exists(dir1)) Directory.CreateDirectory(dir1);
            if (!Directory.Exists(dir2)) Directory.CreateDirectory(dir2);
            if (!Directory.Exists(dir3)) Directory.CreateDirectory(dir3);

            return Path.Combine(baseDir, group, hash.Substring(0, 2), hash.Substring(2, 2), hash.Substring(4, 2), hash.Substring(6) + extension);
        }
    }
}