﻿using System.Collections.Generic;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup
{
    public class Lexer
    {
        private readonly string _input;

        public Lexer(string input)
        {
            _input = Normalize(input);
        }

        public IEnumerable<IToken> Tokens
        {
            get { return TokenEnumerator(_input); }
        }

        private static string Normalize(string input)
        {
            if (input == null)
            {
                return string.Empty;
            }
            else
            {
                return input.Replace("\r\n", "\n")
                    .Replace("\r", "\n")
                    .Trim();
            }
        }

        private static IEnumerable<IToken> TokenEnumerator(string input)
        {
            int i = 0;
            int j = 0;
            int length = input.Length;

            while (i < length)
            {
                if (input[i] == '~')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    yield return new TildeToken();

                    i += 1;
                    j = i;
                }
                else if (input[i] == '=')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '=')
                        k++;

                    yield return new EqualsSignsToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '\n')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '\n')
                        k++;

                    yield return new NewLinesToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '*')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '*')
                        k++;

                    yield return new StarsToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '/')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '/')
                        k++;

                    yield return new SlashesToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '-')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '-')
                        k++;

                    yield return new DashesToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '#')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '#')
                        k++;

                    yield return new SharpsToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '|')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '|')
                        k++;

                    yield return new PipesToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '[')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '[')
                        k++;

                    yield return new LeftBracketsToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == ']')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == ']')
                        k++;

                    yield return new RightBracketsToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '{')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '{')
                        k++;

                    yield return new LeftBracesToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '}')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '}')
                        k++;

                    yield return new RightBracesToken(k);

                    i += k;
                    j = i;
                }
                else if (input[i] == '@')
                {
                    if (i != j)
                    {
                        yield return new TextToken(input.Substring(j, i - j));
                    }

                    int k = 0;
                    while (i + k < length && input[i + k] == '@')
                        k++;

                    yield return new AtsToken(k);

                    i += k;
                    j = i;
                }
                else
                {
                    i++;
                }
            }

            if (j < length)
            {
                yield return new TextToken(input.Substring(j));
            }
        }
    }
}