﻿using System.Collections.Generic;
using Choffmeister.Markup.AST;
using Choffmeister.Markup.State;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup
{
    public class Parser
    {
        private readonly Lexer _lexer;
        private readonly IEnumerable<IToken> _tokens;

        public Parser(Lexer lexer)
        {
            _lexer = lexer;
        }

        public Parser(IEnumerable<IToken> tokens)
        {
            _tokens = tokens;
        }

        public Document Parse()
        {
            IEnumerator<IToken> tokenEnumerator = _tokens != null ? _tokens.GetEnumerator() : _lexer.Tokens.GetEnumerator();

            Document document = new Document();
            DocumentState state = new DocumentState(document);
            bool reuseToken = false;
            bool isLineStart = true;

            while (reuseToken || tokenEnumerator.MoveNext())
            {
                reuseToken = !state.ConsumeToken(tokenEnumerator.Current, isLineStart);

                if (!reuseToken)
                {
                    isLineStart = tokenEnumerator.Current is NewLinesToken || (isLineStart && tokenEnumerator.Current is TextToken && ((TextToken)tokenEnumerator.Current).Text.Trim().Length == 0);
                }
            }

            document.Normalize();

            return document;
        }
    }
}