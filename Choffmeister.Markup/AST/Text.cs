﻿namespace Choffmeister.Markup.AST
{
    public class Text : INode
    {
        public string Content { get; set; }

        public void Normalize()
        {
            this.Content = this.Content ?? string.Empty;
        }
    }
}