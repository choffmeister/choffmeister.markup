﻿using System.Linq;

namespace Choffmeister.Markup.AST
{
    public class Paragraph : ParentNodeBase
    {
        public override void OnNormalize()
        {
            Text firstText = this.Children.FirstOrDefault() as Text;
            Text lastText = this.Children.LastOrDefault() as Text;

            if (firstText != null) firstText.Content = firstText.Content.TrimStart();
            if (lastText != null) lastText.Content = firstText.Content.TrimEnd();
        }
    }
}