﻿using System.Collections.Generic;

namespace Choffmeister.Markup.AST
{
    public class Headline : INode
    {
        public string Content { get; set; }

        public int Level { get; set; }

        public List<int> AbsoluteCounter { get; set; }

        public void Normalize()
        {
            this.Content = this.Content != null ? this.Content.Trim(new char[] { ' ', '=' }) : string.Empty;
        }
    }
}