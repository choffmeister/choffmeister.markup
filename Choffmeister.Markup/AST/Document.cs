﻿namespace Choffmeister.Markup.AST
{
    public class Document : ParentNodeBase
    {
        public string Title { get; set; }

        public string Author { get; set; }

        public override void OnNormalize()
        {
            this.Title = !string.IsNullOrWhiteSpace(this.Title) ? this.Title.Trim() : "Untitled";
            this.Author = !string.IsNullOrWhiteSpace(this.Author) ? this.Author.Trim() : "Unknown";
        }
    }
}