﻿using System.Collections.Generic;
using System.Linq;

namespace Choffmeister.Markup.AST
{
    public class TableOfContents : INode
    {
        private readonly Document _document;

        public IEnumerable<Headline> DocumentHeadlines
        {
            get { return _document.Children.Where(n => n is Headline).Select(n => (Headline)n); }
        }

        public TableOfContents(Document document)
        {
            _document = document;
        }

        public void Normalize()
        {
        }
    }
}