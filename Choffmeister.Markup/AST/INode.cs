﻿namespace Choffmeister.Markup.AST
{
    public interface INode
    {
        void Normalize();
    }
}