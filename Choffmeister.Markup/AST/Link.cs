﻿namespace Choffmeister.Markup.AST
{
    public class Link : INode
    {
        public string Content { get; set; }

        public string Url { get; set; }

        public void Normalize()
        {
            this.Content = this.Content != null ? this.Content.Trim() : string.Empty;
            this.Url = this.Url != null ? this.Url.Trim() : string.Empty;
        }
    }
}