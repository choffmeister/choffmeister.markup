﻿using System.Collections.Generic;

namespace Choffmeister.Markup.AST
{
    public abstract class ParentNodeBase : INode
    {
        private readonly List<INode> _children;

        public List<INode> Children
        {
            get { return _children; }
        }

        public ParentNodeBase()
        {
            _children = new List<INode>();
        }

        public void Normalize()
        {
            foreach (INode child in _children)
            {
                child.Normalize();
            }

            this.OnNormalize();
        }

        public virtual void OnNormalize()
        {
        }
    }
}