﻿namespace Choffmeister.Markup.AST
{
    public class Title : INode
    {
        private readonly Document _document;

        public string DocumentTitle
        {
            get { return _document.Title; }
        }

        public string DocumentAuthor
        {
            get { return _document.Author; }
        }

        public Title(Document document)
        {
            _document = document;
        }

        public void Normalize()
        {
        }
    }
}