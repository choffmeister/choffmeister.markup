﻿using System;
using System.Text;
using Choffmeister.Markup.AST;

namespace Choffmeister.Markup
{
    public class HtmlVisitor : VisitorBase
    {
        private readonly IUrlResolver _urlResolver;
        private readonly StringBuilder _sb;

        public string TableOfContentsHeadline = "Table of contents";

        public string Html { get { return _sb.ToString(); } }

        public HtmlVisitor(IUrlResolver linkResolver = null)
        {
            _urlResolver = linkResolver;
            _sb = new StringBuilder();
        }

        public override void VisitDocumentPre(Document document)
        {
        }

        public override void VisitDocumentPost(Document document)
        {
        }

        public override void VisitHeadline(Headline headline)
        {
            _sb.AppendLine(string.Format("<h{0} id=\"doc-section-{3}\">{2} {1}</h{0}>", Math.Min(headline.Level + 1, 6), headline.Content,
                string.Join(".", headline.AbsoluteCounter), string.Join("-", headline.AbsoluteCounter)));
        }

        public override void VisitParagraphPre(Paragraph paragraph)
        {
            _sb.Append("<p>");
        }

        public override void VisitParagraphPost(Paragraph paragraph)
        {
            _sb.AppendLine("</p>");
        }

        public override void VisitBoldPre(Bold bold)
        {
            _sb.Append("<strong>");
        }

        public override void VisitBoldPost(Bold bold)
        {
            _sb.Append("</strong>");
        }

        public override void VisitItalicPre(Italic italic)
        {
            _sb.Append("<em>");
        }

        public override void VisitItalicPost(Italic italic)
        {
            _sb.Append("</em>");
        }

        public override void VisitOrderedListPre(OrderedList orderedList)
        {
            _sb.AppendLine("<ol>");
        }

        public override void VisitOrderedListPost(OrderedList orderedList)
        {
            _sb.AppendLine("</ol>");
        }

        public override void VisitUnorderedListPre(UnorderedList unorderedList)
        {
            _sb.AppendLine("<ul>");
        }

        public override void VisitUnorderedListPost(UnorderedList unorderedList)
        {
            _sb.AppendLine("</ul>");
        }

        public override void VisitListItemPre(ListItem listItem)
        {
            _sb.Append("<li>");
        }

        public override void VisitListItemPost(ListItem listItem)
        {
            _sb.AppendLine("</li>");
        }

        public override void VisitLink(Link link)
        {
            string url = _urlResolver != null ? _urlResolver.Resolve(link.Url) : link.Url;

            if (string.IsNullOrEmpty(link.Content))
            {
                _sb.Append(string.Format("<a href=\"{0}\">{0}</a>", url));
            }
            else
            {
                _sb.Append(string.Format("<a href=\"{0}\">{1}</a>", url, link.Content));
            }
        }

        public override void VisitText(Text text)
        {
            _sb.Append(text.Content);
        }

        public override void VisitHorizontalRule(HorizontalRule horizontalRule)
        {
            _sb.AppendLine("<hr />");
        }

        public override void VisitTablePre(Table table)
        {
            _sb.Append("<table>");
        }

        public override void VisitTablePost(Table table)
        {
            _sb.AppendLine("</table>");
        }

        public override void VisitTableHeadRowPre(TableHeadRow tableHeadRow)
        {
            _sb.Append("<tr>");
        }

        public override void VisitTableHeadRowPost(TableHeadRow tableHeadRow)
        {
            _sb.Append("</tr>");
        }

        public override void VisitTableRowPre(TableRow tableRow)
        {
            _sb.Append("<tr>");
        }

        public override void VisitTableRowPost(TableRow tableRow)
        {
            _sb.Append("</tr>");
        }

        public override void VisitTableHeadColumnPre(TableHeadColumn tableHeadColumn)
        {
            _sb.Append("<th>");
        }

        public override void VisitTableHeadColumnPost(TableHeadColumn tableHeadColumn)
        {
            _sb.Append("</th>");
        }

        public override void VisitTableColumnPre(TableColumn tableColumn)
        {
            _sb.Append("<td>");
        }

        public override void VisitTableColumnPost(TableColumn tableColumn)
        {
            _sb.Append("</td>");
        }

        public override void VisitTitle(Title title)
        {
            _sb.AppendLine("<h1 class=\"doc-title\">" + title.DocumentTitle + "</h1>");
            _sb.AppendLine("<h3 class=\"doc-author\">" + title.DocumentAuthor + "</h3>");
        }

        public override void VisitTableOfContents(TableOfContents tableOfContents)
        {
            _sb.AppendLine(string.Format("<h1>{0}</h1>", this.TableOfContentsHeadline));
            _sb.AppendLine("<ul class=\"doc-toc\">");

            foreach (Headline headline in tableOfContents.DocumentHeadlines)
            {
                _sb.AppendLine(string.Format("<li class=\"doc-toc-node doc-toc-node-level-{3}\"><a href=\"#doc-section-{2}\">{1} {0}</a></li>", headline.Content,
                    string.Join(".", headline.AbsoluteCounter), string.Join("-", headline.AbsoluteCounter), headline.Level));
            }

            _sb.AppendLine("</ul>");
        }
    }
}