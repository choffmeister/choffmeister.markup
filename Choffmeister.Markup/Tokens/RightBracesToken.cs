﻿namespace Choffmeister.Markup.Tokens
{
    public class RightBracesToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '}'); } }

        public int Count { get { return _count; } }

        public RightBracesToken(int count)
        {
            _count = count;
        }
    }
}