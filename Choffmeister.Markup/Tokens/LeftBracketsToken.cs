﻿namespace Choffmeister.Markup.Tokens
{
    public class LeftBracketsToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '['); } }

        public int Count { get { return _count; } }

        public LeftBracketsToken(int count)
        {
            _count = count;
        }
    }
}