﻿namespace Choffmeister.Markup.Tokens
{
    public class TextToken : IToken
    {
        private readonly string _text;

        public string Text { get { return _text; } }

        public TextToken(string text)
        {
            _text = text;
        }
    }
}