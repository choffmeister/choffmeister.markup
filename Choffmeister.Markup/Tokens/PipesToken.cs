﻿namespace Choffmeister.Markup.Tokens
{
    public class PipesToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '|'); } }

        public int Count { get { return _count; } }

        public PipesToken(int count)
        {
            _count = count;
        }
    }
}