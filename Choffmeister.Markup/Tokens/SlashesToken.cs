﻿namespace Choffmeister.Markup.Tokens
{
    public class SlashesToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '/'); } }

        public int Count { get { return _count; } }

        public SlashesToken(int count)
        {
            _count = count;
        }
    }
}