﻿namespace Choffmeister.Markup.Tokens
{
    public interface IToken
    {
        string Text { get; }
    }
}