﻿namespace Choffmeister.Markup.Tokens
{
    public class StarsToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '*'); } }

        public int Count { get { return _count; } }

        public StarsToken(int count)
        {
            _count = count;
        }
    }
}