﻿namespace Choffmeister.Markup.Tokens
{
    public class DashesToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '-'); } }

        public int Count { get { return _count; } }

        public DashesToken(int count)
        {
            _count = count;
        }
    }
}