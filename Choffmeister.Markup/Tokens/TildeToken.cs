﻿namespace Choffmeister.Markup.Tokens
{
    public class TildeToken : IToken
    {
        public string Text { get { return "~"; } }
    }
}