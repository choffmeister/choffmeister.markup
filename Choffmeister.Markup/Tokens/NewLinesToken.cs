﻿namespace Choffmeister.Markup.Tokens
{
    public class NewLinesToken : IToken
    {
        private readonly int _count;

        public string Text { get { return string.Empty.PadLeft(_count, '\n'); } }

        public int Count { get { return _count; } }

        public NewLinesToken(int count)
        {
            _count = count;
        }
    }
}