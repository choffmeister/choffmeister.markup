﻿using System;
using System.Collections.Generic;
using System.Text;
using Choffmeister.Markup.AST;

namespace Choffmeister.Markup
{
    public class LatexVisitor : VisitorBase
    {
        protected readonly StringBuilder _sb = new StringBuilder();

        protected PdfLatexSetting _documentClass;
        protected List<PdfLatexSetting> _packages;

        public PdfLatexSetting DocumentClass { get { return _documentClass; } set { _documentClass = value; } }

        public List<PdfLatexSetting> Packages { get { return _packages; } }

        public string Latex { get { return _sb.ToString(); } }

        public LatexVisitor()
        {
            _documentClass = new PdfLatexSetting("scrartcl", "a4paper,11pt");
            _packages = new List<PdfLatexSetting>();
            _packages.Add(new PdfLatexSetting("inputenc", "utf8"));
        }

        public override void VisitBoldPost(Bold bold)
        {
        }

        public override void VisitBoldPre(Bold bold)
        {
        }

        public override void VisitDocumentPost(Document document)
        {
            _sb.AppendLine("\\end{document}");
        }

        public override void VisitDocumentPre(Document document)
        {
            if (_documentClass.HasOptions)
            {
                _sb.AppendLine("\\documentclass[" + _documentClass.Options + "]{" + _documentClass.Name + "}");
            }
            else
            {
                _sb.AppendLine("\\documentclass{" + _documentClass.Name + "}");
            }

            foreach (var package in _packages)
            {
                if (package.HasOptions)
                {
                    _sb.AppendLine("\\usepackage[" + package.Options + "]{" + package.Name + "}");
                }
                else
                {
                    _sb.AppendLine("\\usepackage{" + package.Name + "}");
                }
            }

            _sb.AppendLine();
            _sb.AppendLine("\\title{" + document.Title + "}");
            _sb.AppendLine("\\author{" + document.Author + "}");
            _sb.AppendLine();

            _sb.AppendLine("\\begin{document}");
        }

        public override void VisitHeadline(Headline headline)
        {
            string sub = string.Empty;
            for (int i = 0; i < Math.Min(headline.Level, 2); i++)
                sub += "sub";

            _sb.AppendLine(string.Format("\\{1}section{{{0}}}", headline.Content, sub));
        }

        public override void VisitHorizontalRule(HorizontalRule horizontalRule)
        {
        }

        public override void VisitItalicPost(Italic italic)
        {
        }

        public override void VisitItalicPre(Italic italic)
        {
        }

        public override void VisitLink(Link link)
        {
        }

        public override void VisitListItemPost(ListItem listItem)
        {
            _sb.AppendLine();
        }

        public override void VisitListItemPre(ListItem listItem)
        {
            _sb.Append("\\item ");
        }

        public override void VisitOrderedListPost(OrderedList orderedList)
        {
            _sb.AppendLine("\\end{enumerate}");
            _sb.AppendLine();
        }

        public override void VisitOrderedListPre(OrderedList orderedList)
        {
            _sb.AppendLine();
            _sb.AppendLine("\\begin{enumerate}");
        }

        public override void VisitParagraphPost(Paragraph paragraph)
        {
            _sb.AppendLine();
        }

        public override void VisitParagraphPre(Paragraph paragraph)
        {
            _sb.AppendLine();
        }

        public override void VisitTableColumnPost(TableColumn tableColumn)
        {
        }

        public override void VisitTableColumnPre(TableColumn tableColumn)
        {
        }

        public override void VisitTableHeadColumnPost(TableHeadColumn tableHeadColumn)
        {
        }

        public override void VisitTableHeadColumnPre(TableHeadColumn tableHeadColumn)
        {
        }

        public override void VisitTableHeadRowPost(TableHeadRow tableHeadRow)
        {
        }

        public override void VisitTableHeadRowPre(TableHeadRow tableHeadRow)
        {
        }

        public override void VisitTablePost(Table table)
        {
        }

        public override void VisitTablePre(Table table)
        {
        }

        public override void VisitTableRowPost(TableRow tableRow)
        {
        }

        public override void VisitTableRowPre(TableRow tableRow)
        {
        }

        public override void VisitText(Text text)
        {
            _sb.Append(text.Content);
        }

        public override void VisitUnorderedListPost(UnorderedList unorderedList)
        {
            _sb.AppendLine("\\end{itemize}");
            _sb.AppendLine();
        }

        public override void VisitUnorderedListPre(UnorderedList unorderedList)
        {
            _sb.AppendLine();
            _sb.AppendLine("\\begin{itemize}");
        }

        public override void VisitTitle(Title title)
        {
            _sb.AppendLine("\\maketitle");
        }

        public override void VisitTableOfContents(TableOfContents tableOfContent)
        {
            _sb.AppendLine("\\tableofcontents");
        }
    }

    public class PdfLatexSetting
    {
        public string Name { get; set; }

        public string Options { get; set; }

        public bool HasOptions
        {
            get { return this.Options != null; }
        }

        public PdfLatexSetting()
            : this(null)
        {
        }

        public PdfLatexSetting(string name)
            : this(name, null)
        {
        }

        public PdfLatexSetting(string name, string options)
        {
            this.Name = name;
            this.Options = options;
        }
    }
}