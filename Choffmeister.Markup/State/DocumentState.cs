﻿using System.Collections.Generic;
using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class DocumentState : StateBase
    {
        private readonly Document _document;

        protected List<int> _headlineCounter = new List<int>();

        public DocumentState(Document document)
        {
            _document = document;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is NewLinesToken && ((NewLinesToken)token).Count > 1)
            {
                this.PopSuccessor();

                return true;
            }
            else if (isLineStart && this.Successor != null && !(this.Successor is HeadlineState) && token is EqualsSignsToken)
            {
                this.PopSuccessor();

                return false;
            }
            else if (isLineStart && token is TextToken && ((TextToken)token).Text.Trim().Length == 0)
            {
                return true;
            }
            else if (token is DashesToken && ((DashesToken)token).Count >= 4 && isLineStart)
            {
                this.PopSuccessor();

                _document.Children.Add(new HorizontalRule());

                return true;
            }
            else if ((token is StarsToken || token is SharpsToken) && isLineStart && this.Successor != null && !(this.Successor is OrderedListState) && !(this.Successor is UnorderedListState))
            {
                this.PopSuccessor();

                return false;
            }
            else if (token is AtsToken && ((AtsToken)token).Count >= 2 && isLineStart)
            {
                MarkerState state = new MarkerState(_document);
                this.Push(state);

                return true;
            }
            else if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else if (token is EqualsSignsToken)
            {
                Headline node = new Headline();
                HeadlineState state = new HeadlineState(node, _headlineCounter);
                _document.Children.Add(node);
                this.Push(state);

                return false;
            }
            else if (token is StarsToken)
            {
                UnorderedList node = new UnorderedList();
                UnorderedListState state = new UnorderedListState(node);
                _document.Children.Add(node);
                this.Push(state);

                return false;
            }
            else if (token is SharpsToken)
            {
                OrderedList node = new OrderedList();
                OrderedListState state = new OrderedListState(node);
                _document.Children.Add(node);
                this.Push(state);

                return false;
            }
            else
            {
                Paragraph node = new Paragraph();
                ParagraphState state = new ParagraphState(node);
                _document.Children.Add(node);
                this.Push(state);

                return false;
            }
        }
    }
}