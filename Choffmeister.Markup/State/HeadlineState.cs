﻿using System.Collections.Generic;
using System.Linq;
using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class HeadlineState : StateBase
    {
        private readonly Headline _headline;
        private readonly List<int> _headlineCounter;

        private bool _contentStarted = false;

        public HeadlineState(Headline headline, List<int> headlineCounter)
        {
            _headline = headline;
            _headlineCounter = headlineCounter;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is EqualsSignsToken && !_contentStarted)
            {
                _contentStarted = true;
                _headline.Level = ((EqualsSignsToken)token).Count - 1;

                if (_headlineCounter.Count > _headline.Level)
                    _headlineCounter.RemoveRange(_headline.Level + 1, _headlineCounter.Count - _headline.Level - 1);
                if (_headlineCounter.Count <= _headline.Level)
                    _headlineCounter.AddRange(Enumerable.Repeat(0, _headline.Level - _headlineCounter.Count + 1));
                _headlineCounter[_headline.Level]++;
                _headline.AbsoluteCounter = _headlineCounter.ToList();

                return true;
            }
            else if (token is NewLinesToken)
            {
                this.PopThis();

                return true;
            }
            else
            {
                _headline.Content += token.Text;

                return true;
            }
        }
    }
}