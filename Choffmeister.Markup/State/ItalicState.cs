﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class ItalicState : InlineState
    {
        private readonly Italic _italic;

        public ItalicState(Italic italic)
            : base(italic)
        {
            _italic = italic;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is SlashesToken && ((SlashesToken)token).Count > 1)
            {
                this.PopThis();

                return true;
            }
            else if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else
            {
                return base.ConsumeToken(token, isLineStart);
            }
        }
    }
}