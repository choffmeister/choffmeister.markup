﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class TextState : StateBase
    {
        private readonly Text _text;

        public TextState(Text text)
        {
            _text = text;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is StarsToken && ((StarsToken)token).Count > 1)
            {
                this.PopThis();

                return false;
            }
            else if (token is SlashesToken && ((SlashesToken)token).Count > 1)
            {
                this.PopThis();

                return false;
            }
            else if (token is LeftBracketsToken && ((LeftBracketsToken)token).Count > 1)
            {
                this.PopThis();

                return false;
            }
            else if (token is NewLinesToken && ((NewLinesToken)token).Count == 1)
            {
                _text.Content += " ";

                return true;
            }
            else
            {
                _text.Content += token.Text;

                return true;
            }
        }
    }
}