﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class UnorderedListState : StateBase
    {
        private readonly UnorderedList _unorderedList;
        private int _starCount;

        public UnorderedListState(UnorderedList unorderedList)
        {
            _unorderedList = unorderedList;
            _starCount = 0;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (_starCount == 0)
            {
                _starCount = ((StarsToken)token).Count;

                return true;
            }
            else if (isLineStart && token is StarsToken && ((StarsToken)token).Count == _starCount)
            {
                this.PopSuccessor();

                ListItem node = new ListItem();
                ListItemState state = new ListItemState(node);
                _unorderedList.Children.Add(node);
                this.Push(state);

                return true;
            }
            else if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else if (isLineStart && token is StarsToken)
            {
                UnorderedList node = new UnorderedList();
                UnorderedListState state = new UnorderedListState(node);
                _unorderedList.Children.Add(node);
                this.Push(state);

                return false;
            }
            else if (isLineStart && token is SharpsToken)
            {
                OrderedList node = new OrderedList();
                OrderedListState state = new OrderedListState(node);
                _unorderedList.Children.Add(node);
                this.Push(state);

                return false;
            }
            else
            {
                ListItem node = new ListItem();
                ListItemState state = new ListItemState(node);
                _unorderedList.Children.Add(node);
                this.Push(state);

                return false;
            }
        }
    }
}