﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class LinkState : StateBase
    {
        private readonly Link _link;
        private bool _contentStarted;

        public LinkState(Link link)
        {
            _link = link;
            _contentStarted = false;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is RightBracketsToken && ((RightBracketsToken)token).Count > 1)
            {
                this.PopThis();

                return true;
            }
            else if (token is PipesToken && !_contentStarted)
            {
                _contentStarted = true;

                return true;
            }
            else if (!_contentStarted)
            {
                _link.Url += token.Text;

                return true;
            }
            else
            {
                _link.Content += token.Text;

                return true;
            }
        }
    }
}