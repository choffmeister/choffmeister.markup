﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class OrderedListState : StateBase
    {
        private readonly OrderedList _orderedList;
        private int _sharpCount;

        public OrderedListState(OrderedList orderedList)
        {
            _orderedList = orderedList;
            _sharpCount = 0;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (_sharpCount == 0)
            {
                _sharpCount = ((SharpsToken)token).Count;

                return true;
            }
            else if (isLineStart && token is SharpsToken && ((SharpsToken)token).Count == _sharpCount)
            {
                this.PopSuccessor();

                ListItem node = new ListItem();
                ListItemState state = new ListItemState(node);
                _orderedList.Children.Add(node);
                this.Push(state);

                return true;
            }
            else if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else if (isLineStart && token is SharpsToken)
            {
                OrderedList node = new OrderedList();
                OrderedListState state = new OrderedListState(node);
                _orderedList.Children.Add(node);
                this.Push(state);

                return false;
            }
            else if (isLineStart && token is StarsToken)
            {
                UnorderedList node = new UnorderedList();
                UnorderedListState state = new UnorderedListState(node);
                _orderedList.Children.Add(node);
                this.Push(state);

                return false;
            }
            else
            {
                ListItem node = new ListItem();
                ListItemState state = new ListItemState(node);
                _orderedList.Children.Add(node);
                this.Push(state);

                return false;
            }
        }
    }
}