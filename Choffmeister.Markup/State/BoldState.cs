﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class BoldState : InlineState
    {
        private readonly Bold _bold;

        public BoldState(Bold bold)
            : base(bold)
        {
            _bold = bold;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is StarsToken && ((StarsToken)token).Count > 1)
            {
                this.PopThis();

                return true;
            }
            else if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else
            {
                return base.ConsumeToken(token, isLineStart);
            }
        }
    }
}