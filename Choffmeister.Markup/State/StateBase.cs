﻿using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public abstract class StateBase
    {
        private StateBase _precossor;
        private StateBase _successor;

        public StateBase Precessor
        {
            get { return _precossor; }
        }

        public StateBase Successor
        {
            get { return _successor; }
        }

        public void Push(StateBase state)
        {
            _successor = state;
            state._precossor = this;
        }

        public void PopThis()
        {
            _precossor._successor = null;
            _precossor = null;
        }

        public void PopSuccessor()
        {
            if (_successor != null)
            {
                _successor._precossor = null;
                _successor = null;
            }
        }

        public abstract bool ConsumeToken(IToken token, bool isLineStart);
    }
}