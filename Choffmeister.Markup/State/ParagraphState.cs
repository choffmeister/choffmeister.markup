﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class ParagraphState : InlineState
    {
        private readonly Paragraph _paragraph;

        public ParagraphState(Paragraph paragraph)
            : base(paragraph)
        {
            _paragraph = paragraph;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else
            {
                return base.ConsumeToken(token, isLineStart);
            }
        }
    }
}