﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class InlineState : StateBase
    {
        private readonly ParentNodeBase _parent;

        public InlineState(ParentNodeBase parent)
        {
            _parent = parent;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else if (token is StarsToken && ((StarsToken)token).Count > 1)
            {
                Bold node = new Bold();
                BoldState state = new BoldState(node);
                _parent.Children.Add(node);
                this.Push(state);

                return true;
            }
            else if (token is SlashesToken && ((SlashesToken)token).Count > 1)
            {
                Italic node = new Italic();
                ItalicState state = new ItalicState(node);
                _parent.Children.Add(node);
                this.Push(state);

                return true;
            }
            else if (token is LeftBracketsToken && ((LeftBracketsToken)token).Count > 1)
            {
                Link node = new Link();
                LinkState state = new LinkState(node);
                _parent.Children.Add(node);
                this.Push(state);

                return true;
            }
            else
            {
                Text node = new Text();
                TextState state = new TextState(node);
                _parent.Children.Add(node);
                this.Push(state);

                return false;
            }
        }
    }
}