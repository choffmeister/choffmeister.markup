﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class MarkerState : StateBase
    {
        private readonly Document _document;

        private MarkerType _type;

        public MarkerState(Document document)
        {
            _document = document;
            _type = MarkerType.None;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (token is TextToken && _type == MarkerType.None)
            {
                if (token.Text.Trim().StartsWith("TITLE "))
                {
                    _document.Children.Add(new Title(_document));
                    _document.Title = token.Text.Trim().Substring(6);
                    _type = MarkerType.Title;
                }
                else if (token.Text.Trim() == "TABLEOFCONTENTS")
                {
                    _document.Children.Add(new TableOfContents(_document));
                    _type = MarkerType.TableOfContents;
                }
                else if (token.Text.Trim().StartsWith("AUTHOR "))
                {
                    _document.Author = token.Text.Trim().Substring(7);
                    _type = MarkerType.Author;
                }

                return true;
            }
            else if (token is NewLinesToken)
            {
                this.PopThis();

                return true;
            }
            else
            {
                return true;
            }
        }
    }

    public enum MarkerType
    {
        None,
        Title,
        Author,
        TableOfContents,
    }
}