﻿using Choffmeister.Markup.AST;
using Choffmeister.Markup.Tokens;

namespace Choffmeister.Markup.State
{
    public class ListItemState : InlineState
    {
        private readonly ListItem _listItem;

        public ListItemState(ListItem listItem)
            : base(listItem)
        {
            _listItem = listItem;
        }

        public override bool ConsumeToken(IToken token, bool isLineStart)
        {
            if (isLineStart && (token is StarsToken || token is SharpsToken))
            {
                this.PopThis();

                return false;
            }
            else if (this.Successor != null)
            {
                return this.Successor.ConsumeToken(token, isLineStart);
            }
            else
            {
                return base.ConsumeToken(token, isLineStart);
            }
        }
    }
}