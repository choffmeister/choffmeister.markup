﻿using System;
using Choffmeister.Markup.AST;

namespace Choffmeister.Markup
{
    public abstract class VisitorBase
    {
        public void Visit(INode node)
        {
            if (node is Document)
            {
                Document document = (Document)node;

                this.VisitDocumentPre(document);

                foreach (INode child in document.Children)
                {
                    this.Visit(child);
                }

                this.VisitDocumentPost(document);
            }
            else if (node is Headline)
            {
                Headline headline = (Headline)node;

                this.VisitHeadline(headline);
            }
            else if (node is Paragraph)
            {
                Paragraph paragraph = (Paragraph)node;

                this.VisitParagraphPre(paragraph);

                foreach (INode child in paragraph.Children)
                {
                    this.Visit(child);
                }

                this.VisitParagraphPost(paragraph);
            }
            else if (node is Bold)
            {
                Bold bold = (Bold)node;

                this.VisitBoldPre(bold);

                foreach (INode child in bold.Children)
                {
                    this.Visit(child);
                }

                this.VisitBoldPost(bold);
            }
            else if (node is Italic)
            {
                Italic italic = (Italic)node;

                this.VisitItalicPre(italic);

                foreach (INode child in italic.Children)
                {
                    this.Visit(child);
                }

                this.VisitItalicPost(italic);
            }
            else if (node is OrderedList)
            {
                OrderedList orderedList = (OrderedList)node;

                this.VisitOrderedListPre(orderedList);

                foreach (INode child in orderedList.Children)
                {
                    this.Visit(child);
                }

                this.VisitOrderedListPost(orderedList);
            }
            else if (node is UnorderedList)
            {
                UnorderedList unorderedList = (UnorderedList)node;

                this.VisitUnorderedListPre(unorderedList);

                foreach (INode child in unorderedList.Children)
                {
                    this.Visit(child);
                }

                this.VisitUnorderedListPost(unorderedList);
            }
            else if (node is ListItem)
            {
                ListItem listItem = (ListItem)node;

                this.VisitListItemPre(listItem);

                foreach (INode child in listItem.Children)
                {
                    this.Visit(child);
                }

                this.VisitListItemPost(listItem);
            }
            else if (node is Link)
            {
                Link link = (Link)node;

                this.VisitLink(link);
            }
            else if (node is Text)
            {
                Text text = (Text)node;

                this.VisitText(text);
            }
            else if (node is HorizontalRule)
            {
                HorizontalRule horizontalRule = (HorizontalRule)node;

                this.VisitHorizontalRule(horizontalRule);
            }
            else if (node is Table)
            {
                Table table = (Table)node;

                this.VisitTablePre(table);

                foreach (INode child in table.Children)
                {
                    this.Visit(child);
                }

                this.VisitTablePost(table);
            }
            else if (node is TableHeadRow)
            {
                TableHeadRow tableHeadRow = (TableHeadRow)node;

                this.VisitTableHeadRowPre(tableHeadRow);

                foreach (INode child in tableHeadRow.Children)
                {
                    this.Visit(child);
                }

                this.VisitTableHeadRowPost(tableHeadRow);
            }
            else if (node is TableRow)
            {
                TableRow tableRow = (TableRow)node;

                this.VisitTableRowPre(tableRow);

                foreach (INode child in tableRow.Children)
                {
                    this.Visit(child);
                }

                this.VisitTableRowPost(tableRow);
            }
            else if (node is TableHeadColumn)
            {
                TableHeadColumn tableHeadColumn = (TableHeadColumn)node;

                this.VisitTableHeadColumnPre(tableHeadColumn);

                foreach (INode child in tableHeadColumn.Children)
                {
                    this.Visit(child);
                }

                this.VisitTableHeadColumnPost(tableHeadColumn);
            }
            else if (node is TableColumn)
            {
                TableColumn tableColumn = (TableColumn)node;

                this.VisitTableColumnPre(tableColumn);

                foreach (INode child in tableColumn.Children)
                {
                    this.Visit(child);
                }

                this.VisitTableColumnPost(tableColumn);
            }
            else if (node is Title)
            {
                Title title = (Title)node;

                this.VisitTitle(title);
            }
            else if (node is TableOfContents)
            {
                TableOfContents tableOfContents = (TableOfContents)node;

                this.VisitTableOfContents(tableOfContents);
            }
            else
            {
                throw new NotSupportedException(string.Format("Nodes of type '{0}' are not supported.", node.GetType().FullName));
            }
        }

        public abstract void VisitDocumentPre(Document document);

        public abstract void VisitDocumentPost(Document document);

        public abstract void VisitHeadline(Headline headline);

        public abstract void VisitParagraphPre(Paragraph paragraph);

        public abstract void VisitParagraphPost(Paragraph paragraph);

        public abstract void VisitBoldPre(Bold bold);

        public abstract void VisitBoldPost(Bold bold);

        public abstract void VisitItalicPre(Italic italic);

        public abstract void VisitItalicPost(Italic italic);

        public abstract void VisitOrderedListPre(OrderedList orderedList);

        public abstract void VisitOrderedListPost(OrderedList orderedList);

        public abstract void VisitUnorderedListPre(UnorderedList unorderedList);

        public abstract void VisitUnorderedListPost(UnorderedList unorderedList);

        public abstract void VisitListItemPre(ListItem listItem);

        public abstract void VisitListItemPost(ListItem listItem);

        public abstract void VisitLink(Link link);

        public abstract void VisitText(Text text);

        public abstract void VisitHorizontalRule(HorizontalRule horizontalRule);

        public abstract void VisitTablePre(Table table);

        public abstract void VisitTablePost(Table table);

        public abstract void VisitTableHeadRowPre(TableHeadRow tableHeadRow);

        public abstract void VisitTableHeadRowPost(TableHeadRow tableHeadRow);

        public abstract void VisitTableRowPre(TableRow tableRow);

        public abstract void VisitTableRowPost(TableRow tableRow);

        public abstract void VisitTableHeadColumnPre(TableHeadColumn tableHeadColumn);

        public abstract void VisitTableHeadColumnPost(TableHeadColumn tableHeadColumn);

        public abstract void VisitTableColumnPre(TableColumn tableColumn);

        public abstract void VisitTableColumnPost(TableColumn tableColumn);

        public abstract void VisitTitle(Title title);

        public abstract void VisitTableOfContents(TableOfContents tableOfContents);
    }
}